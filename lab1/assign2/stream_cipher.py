from config import return_keystream_generator, return_key, return_seed
from blum_blum_shub import BlumBlumShub
from solitaire import Solitaire


class StreamCypher:
    def __init__(self):
        """
        Kulcsfolyamgeneralo a config allomanybol kiolvassa, hogy a ket kliens
        milyen generalo algoritmust hasznalnak az uzenetvaltasok soran, s az
        ahhoz a generalohoz tartozo kulcsot is:
            - key -> Solitaire tabla beallitasahoz szukseges kulcs
            - seed -> Blum-Blum-Shub generalohoz szukseges kulcs
        """
        generator = return_keystream_generator()
        if generator == Solitaire:
            key = return_key()
            self.keystream_generator = generator(key)
        elif generator == BlumBlumShub:
            seed = return_seed()
            self.keystream_generator = generator(seed)

    def encrypt_text(self, plaintext):
        """
        General egy a megadott szoveggel egyenlo hosszu kulcsfolyamot, amit byte-onkent
        XOR-oz a megadott szoveggel.
        Vegeredmeny a titkositott szoveg byteokban.
        """
        keystream = self.keystream_generator.get_keystream(len(plaintext))
        ciphertext = []

        for char, byte in zip([x for x in plaintext], keystream):
            ciphertext.append(byte ^ char)

        return bytes(ciphertext)

    def decrypt_text(self, ciphertext):
        """
        A dekriptalas az XOR muveletnek koszonhetoen ugyanugy mukodik, mint az enkriptalas.
        """
        return self.encrypt_text(ciphertext)

    def synchronize(self, length):
        """
        Ha az offset miatt valamelyk kliens le van maradva(nem kapott meg bizonyos uzeneteket),
        akkor a tablajat szikronizalni kell, azaz ha meg legeneralok egy olyan hosszusagu
        kulcsfolyamot amennyivel le van maradva, akkor pont szinkronba lesz,
        s onnan johet a decrypt/encrypt.
        """
        self.keystream_generator.synchronize(length)
