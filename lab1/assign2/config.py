from blum_blum_shub import BlumBlumShub
from solitaire import Solitaire


def return_key():
    return "secretkey"


def return_keystream_generator():
    return Solitaire


def return_seed():
    return 2061211491
