from utils import to_number, byte_to_bits, bits_to_byte

WHITE = 53
BLACK = 54


class Solitaire:
    def __init__(self, key):
        """
        Ha nincs megadva kulcs, akkor a kezdeti tablarendezessel dolgozunk,
        kulonben beallitjuk a tablat a kulcs szerint.
        """
        self.deck = list(range(1, 55))

        if key != "":
            self.set_deck_with_given_key(key)

    def set_deck_with_given_key(self, key):
        """
        Tabla beallitasa kulcs szerint.
        """
        for char in key:
            self.first_four_steps()
            self.fourth_step_count_cut(to_number(char))

    def first_step_down_1(self, joker):
        """
        Joker 1 kartyaval lentebb valo vitele.
        """
        joker_index = self.deck.index(joker)

        if joker_index < 53:
            self.deck[joker_index], self.deck[joker_index + 1] = (
                self.deck[joker_index + 1],
                self.deck[joker_index],
            )
        else:  # the joker is the last one
            self.deck[1:] = self.deck[-1:] + self.deck[1:-1]

    def second_step_down_2(self):
        """
        Fekete Joker 2 kartyaval lentebb valo vitele.
        """
        self.first_step_down_1(BLACK)
        self.first_step_down_1(BLACK)

    def third_step_triple_cut(self):
        """
        A ket Jokeren kivuli reszek felcserelese egymas kozott.
        """
        first_joker_index = self.deck.index(WHITE)
        second_joker_index = self.deck.index(BLACK)

        if first_joker_index > second_joker_index:
            first_joker_index, second_joker_index = (
                second_joker_index,
                first_joker_index,
            )

        self.deck[:] = (
            self.deck[second_joker_index + 1 :]
            + self.deck[first_joker_index : second_joker_index + 1]
            + self.deck[:first_joker_index]
        )

    def fourth_step_count_cut(self, char):
        """
        Az utolso kartya altal mutatott darab szam kartya leszamolasa, s azok felcserelese a
        leszamolt kartyal s az utolso kartya kozti kartyakkal.
        """
        card_number = min(char, 53)

        self.deck[:-1] = self.deck[card_number:-1] + self.deck[:card_number]

    def first_four_steps(self):
        """
        Tabla rendezese: elso 4 lepes.
        """
        # 1. step: move the white joker 1 card down
        self.first_step_down_1(WHITE)

        # 2. step: move the black joker 2 cards down
        self.second_step_down_2()

        # 3. step: perform a triple cut
        self.third_step_triple_cut()

        # 4. step: perform a count cut based on the last card
        self.fourth_step_count_cut(self.deck[-1])

    def generate_number(self):
        """
        -Tabla rendezese: 5 lepes.
        -Elvegezzuk az elso 4 lepest, majd a felso kartyanak megfelelo darab kartyat leszamolunk,
        s az ezek utani lesz a vegso kimenet.
        -Ha a vegso kimenet egy jokernek megfelelo szam, akkor probaljuk ujra rendezni a tablankat,
        amig egy nem jokernek megfelelo szamot kapunk.
        """
        while 1:
            self.first_four_steps()

            # 5. step: get output number
            card_number = min(self.deck[0], 53)
            if self.deck[card_number] < 53:
                return self.deck[card_number]

    def get_keystream(self, length):
        """
        -Egy length hosszusagu kulcsfolyam generalasa.
        -Mivel a szamgeneralo 1 es 52 kozotti szamot dobhat vissza, igy nem erintjuk egy
        byte mind a 8 bitjet, igy osszetessszuk ezt a 8 bitet 2 szam segitsegevel.
        4 alacsonyabb poziciokon levo biteket kiveszem mindket szambol s letrehozok
        egy rendes byteot, ami vegul a kulcsfolyamba kerul.
        """
        keystream = []

        for _ in range(length):
            byte_in_bits = []

            solitaire_number = self.generate_number()
            byte_in_bits += byte_to_bits(solitaire_number)[-4:]

            solitaire_number = self.generate_number()
            byte_in_bits += byte_to_bits(solitaire_number)[-4:]

            byte = bits_to_byte(byte_in_bits)

            keystream.append(byte)

        return bytes(keystream)

    def synchronize(self, length):
        """
        Ha az offset miatt valamelyk kliens le van maradva(nem kapott meg bizonyos uzeneteket),
        akkor a tablajat szikronizalni kell, azaz ha meg legeneralok egy olyan hosszusagu
        kulcsfolyamot amennyivel le van maradva, akkor pont szinkronba lesz,
        s onnan johet a decrypt/encrypt.
        """
        self.get_keystream(length)
