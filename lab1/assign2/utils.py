import string


class Error(Exception):
    """Base class for exceptions in this module."""


class BinaryConversionError(Error):
    """Custom exception for invalid binary conversions."""


def byte_to_bits(byte):
    if not 0 <= byte <= 255:
        raise BinaryConversionError(byte)

    out = []
    for _ in range(8):
        out.append(byte & 1)
        byte >>= 1
    return out[::-1]


def bits_to_byte(bits):
    if not all(bit == 0 or bit == 1 for bit in bits):
        raise BinaryConversionError("Invalid bitstring passed")

    byte = 0
    for bit in bits:
        byte *= 2
        if bit:
            byte += 1
    return byte


def to_number(char):
    if char in string.ascii_lowercase:
        return ord(char) - 96

    if char in string.ascii_uppercase:
        return ord(char) - 38
