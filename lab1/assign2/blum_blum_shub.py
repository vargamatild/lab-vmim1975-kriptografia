from utils import bits_to_byte

# pylint: disable=too-few-public-methods
# pylint: disable=invalid-name
class BlumBlumShub:
    def __init__(self, key):
        """
        Kezdeti parameterek beallitasa. p, q nagyon nagy primek, x pedig a kezdeti "random" szam.
        """
        self.p = 30000000091
        self.q = 40000000003
        self.seed = key
        self.n = self.p * self.q
        self.x = (self.seed * self.seed) % self.n

    def get_keystream(self, length):
        """
        Egy megadott algoritmus alapjan general egy megadott hosszu kulcsfolyamot.
        """
        keystream = []

        for _ in range(length):
            byte = []

            for _ in range(8):
                byte.append(self.x % 2)
                self.x = (self.x * self.x) % self.n

            keystream.append(bits_to_byte(byte))

        return bytes(keystream)

    def synchronize(self, length):
        """
        Ha az offset miatt valamelyk kliens le van maradva(nem kapott meg bizonyos uzeneteket),
        akkor a tablajat szikronizalni kell, azaz ha meg legeneralok egy olyan hosszusagu
        kulcsfolyamot amennyivel le van maradva, akkor pont szinkronba lesz,
        s onnan johet a decrypt/encrypt.
        """
        self.get_keystream(length)
