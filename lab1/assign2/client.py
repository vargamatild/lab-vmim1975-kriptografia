import sys
import json
from socket import AF_INET, SOCK_STREAM, socket
from threading import Thread
from stream_cipher import StreamCypher

NAME = "localhost"
PORT = 8080

# pylint: disable=broad-except
# pylint: disable=too-many-instance-attributes
class Client:
    def __init__(self, server_or_client_parameter):
        """
        Konzolrol megadott parameter (1 | 2) alapjan eldontodik,
        hogy melyk kliens az akire kapcsolodhat a masik kliens.
        S ennek megfeleloen jon letre a szerver-kliens s a kliens.
        """
        self.server_or_client = server_or_client_parameter
        self.receive_message = True
        self.send_message = True
        self.close = False
        self.cipher = StreamCypher()
        self.offset = 0

        if self.server_or_client == "1":
            self.client_socket = socket(AF_INET, SOCK_STREAM)
            self.client_socket.bind((NAME, PORT))
            self.client_socket.listen(1)
            print(
                "The (server) client is running. \nWaiting for one more client to connect ...\n"
            )
        else:
            self.connected_client = socket(AF_INET, SOCK_STREAM)
            self.connected_client.connect((NAME, PORT))
            print("The client is connected. \n")

    def receive(self):
        """
        -Uzenetek fogadasa, akkor ha az illeton van a sor s meg nincs senki kilepve a kapcsolatbol.
        -Json formatumban a socketbol kiolvasom az uzenetet, abbol az offsetnek megfeleloen
        szikronizalom a tablat, ha kell, majd a megfelelo kulcsfolyamgeneralo felhasznalasaval
        dekodolom az enkriptalt szoveget.
        -Ha az enkriptalt szoveg az "LOGOUT" volt, akkor azt jelenti, hogy az a kliens aki ezt
        kuldte kilepett, illetve ennek kovetkezmenye az, hogy megszunik a kapcsolat kettejuk kozott.
        """
        while True:
            if self.receive_message:
                try:
                    data = json.loads(self.connected_client.recv(1024))
                    encrypted_message = data["encrypted_message"]
                    encrypted_message = bytes([ord(x) for x in encrypted_message])

                    print(
                        "Received encrypted message:",
                        encrypted_message,
                        "and offset:",
                        data["offset"],
                    )

                    if data["offset"] == self.offset + len(encrypted_message):
                        self.offset = data["offset"]
                        message = self.cipher.decrypt_text(encrypted_message).decode()
                    elif data["offset"] > self.offset + len(encrypted_message):
                        self.cipher.synchronize(
                            data["offset"] - self.offset - len(encrypted_message)
                        )
                        self.offset = data["offset"]
                        message = self.cipher.decrypt_text(encrypted_message).decode()

                    print(f"Message received: {message}\n\n")

                    if message != "" and message == "LOGOUT":
                        print("Client left the chat. The connection is over.")
                        self.receive_message = False
                        self.send_message = False
                        self.close = True
                        break

                    if message not in ["", "LOGOUT"]:
                        self.receive_message = False
                        self.send_message = True
                except Exception as exception:
                    print("Something went wrong!\n", str(exception))
                    self.receive_message = False
                    self.send_message = False
                    self.close = True
                    break
            elif self.close:
                break

    def write(self):
        """
        -Uzenetek kuldese, akkor ha az illeton van a sor s meg nincs senki kilepve a kapcsolatbol.
        -Konzolrol beolvasom az uzenetet, majd a megfelelo kulcsfolyamgeneralo felhasznalasaval
        enkodolom az uzenetet, majd Json formatumban a socketbe beirom az uzenetet s az offsetet.
        -Ha a beolvasott uzenet az "LOGOUT" volt, akkor azt jelenti, hogy az a kliens aki ezt
        kuldte kilepett, illetve ennek kovetkezmenye az, hogy megszunik a kapcsolat kettejuk kozott.
        """
        while True:
            if self.send_message:
                message = input("Enter your message: ")
                encrypted_message = self.cipher.encrypt_text(message.encode())

                if message != "":
                    self.offset += len(encrypted_message)

                    data = dict()
                    data["encrypted_message"] = "".join(
                        [chr(x) for x in encrypted_message]
                    )
                    data["offset"] = self.offset
                    self.connected_client.send(json.dumps(data).encode("ascii"))
                    print(
                        "Sent encrypted message:",
                        encrypted_message,
                        "and offset:",
                        data["offset"],
                        "\n\n",
                    )

                    self.send_message = False

                    if message == "LOGOUT":
                        print("Client left the chat. The connection is over.")
                        self.close = True
                        self.send_message = False
                        self.receive_message = False
                        break

                    self.receive_message = True
            elif self.close:
                break

    def main(self):
        """
        Ha a parameterkent megadott szam az "1", akkor szerver-kliens van,
        o varja azt hogy valaki rakapcsolodjon, s o kezdi az uzenetvaltast azzal, hogy
        o kuld elobb a kapcsolodott kliens varja az elso uzenetet, s majd ez megfordul.
        """
        if self.server_or_client == "1":
            self.connected_client, client_address = self.client_socket.accept()
            print(f"Client arrived with {client_address} address.\n\n")
            self.receive_message = False
            self.send_message = True
        else:
            self.receive_message = True
            self.send_message = False

        receive_thread = Thread(target=self.receive)
        receive_thread.start()

        write_thread = Thread(target=self.write)
        write_thread.start()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python client.py [1 (main client) | 2 (normal client)]")
    elif len(sys.argv) == 2:
        server_or_client = sys.argv[1]
        if server_or_client in ["1", "2"]:
            Client(sys.argv[1]).main()
        else:
            print("Usage: python client.py [1 (main client) | 2 (normal client)]")
