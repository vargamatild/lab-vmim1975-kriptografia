#!/usr/bin/env python3 -tt
"""
File: crypto-console.py
-----------------------
Implements a console menu to interact with the cryptography functions exported
by the crypto module.

If you are a student, you shouldn't need to change anything in this file.
"""
import os.path
import random
from crypto import (
    encrypt_caesar,
    encrypt_caesar_binary_data,
    decrypt_caesar,
    encrypt_vigenere,
    encrypt_vigenere_binary_data,
    decrypt_vigenere,
    generate_private_key,
    create_public_key,
    encrypt_mh,
    decrypt_mh,
    encrypt_scytale,
    encrypt_scytale_binary_data,
    decrypt_scytale,
    encrypt_railfence,
    encrypt_railfence_binary_data,
    decrypt_railfence,
)


#############################
# GENERAL CONSOLE UTILITIES #
#############################


def get_tool():
    print("* Tool *")
    return _get_selection(
        "(C)aesar, (V)igenere, (M)erkle-Hellman, (S)cytale or (R)ailfence? ", "CVMSR"
    )


def get_action():
    """Return true iff encrypt"""
    print("* Action *")
    return _get_selection("(E)ncrypt or (D)ecrypt? ", "ED")


def get_filename():
    filename = ""

    while not filename:
        filename = input("Filename (path)? ")

        if filename:
            if os.path.isfile(filename):
                break
            else:
                print("This is not an existing file! Try again!")
                filename = ""

    return filename


def get_input(binary=False):
    print("* Input *")
    choice = _get_selection("(F)ile or (S)tring? ", "FS")
    if choice == "S":
        text = input("Enter a string: ").strip().upper()
        while not text:
            text = input("Enter a string: ").strip().upper()
        if binary:
            return bytes(text, encoding="utf8")
        return text
    else:
        filename = get_filename()

        flags = "r"
        if binary:
            flags += "b"
        with open(filename, flags) as infile:
            return infile.read()


def set_output(output, binary=False):
    print("* Output *")
    choice = _get_selection("(F)ile or (S)tring? ", "FS")
    if choice == "S":
        print(output)
    else:
        filename = get_filename()

        flags = "w"
        if binary:
            flags += "b"
        with open(filename, flags) as outfile:
            print("Writing data to {}...".format(filename))
            outfile.write(output)


def _get_selection(prompt, options):
    choice = input(prompt).upper()
    while not choice or choice[0] not in options:
        choice = input(
            "Please enter one of {}. {}".format("/".join(options), prompt)
        ).upper()
    return choice[0]


def get_yes_or_no(prompt, reprompt=None):
    """
    Asks the user whether they would like to continue.
    Responses that begin with a `Y` return True. (case-insensitively)
    Responses that begin with a `N` return False. (case-insensitively)
    All other responses (including '') cause a reprompt.
    """
    if not reprompt:
        reprompt = prompt

    choice = input("{} (Y/N) ".format(prompt)).upper()
    while not choice or choice[0] not in ["Y", "N"]:
        choice = input(
            "Please enter either 'Y' or 'N'. {} (Y/N)? ".format(reprompt)
        ).upper()
    return choice[0] == "Y"


def clean_caesar(text):
    """Convert text to a form compatible with the preconditions imposed by Caesar cipher"""
    return text.upper()


def clean_vigenere(text):
    """Convert text to a form compatible with the preconditions imposed by Vigenere cipher"""
    return "".join(ch for ch in text.upper() if ch.isupper())


def clean_scytale(text):
    """Convert text to a form compatible with the preconditions imposed by Scytale cipher"""
    return "".join(ch for ch in text.upper() if ch.isupper() or ch == ".")


def clean_railfence(text):
    """Convert text to a form compatible with the preconditions imposed by Railfence cipher"""
    return text.upper()


def run_caesar():
    """run Caesar cipher"""
    action = get_action()
    encrypting = action == "E"
    binary_data = _get_selection(
        "Do you want to read binary data? (Y)es or (N)o ", "YN"
    )

    if binary_data == "Y":
        data = get_input(binary=True)
    else:
        data = clean_caesar(get_input(binary=False))

    print("* Transform *")

    if binary_data == "Y" and encrypting:
        output = encrypt_caesar_binary_data(data)
        set_output(bytes(output, "utf-8"), binary=True)
    else:
        output = (encrypt_caesar if encrypting else decrypt_caesar)(data)
        set_output(output)


def run_vigenere():
    """run Vigenere cipher"""
    action = get_action()
    encrypting = action == "E"
    binary_data = _get_selection(
        "Do you want to read binary data? (Y)es or (N)o ", "YN"
    )

    if binary_data == "Y":
        data = get_input(binary=True)
    else:
        data = clean_vigenere(get_input())

    print("* Transform *")
    print("Keyword? ")
    if binary_data == "Y":
        keyword = get_input(binary=True)
    else:
        keyword = clean_vigenere(get_input())

    if binary_data == "Y" and encrypting:
        output = encrypt_vigenere_binary_data(data, keyword)
        set_output(bytes(output, "utf-8"), binary=True)
    else:
        output = (encrypt_vigenere if encrypting else decrypt_vigenere)(
            data, keyword)
        set_output(output)


def run_merkle_hellman():
    """run Merkle-Hellman cipher"""
    action = get_action()

    print("* Seed *")
    seed = input("Set Seed [enter for random]: ")

    if not seed:
        random.seed()
    else:
        random.seed(seed)

    print("* Building private key...")

    private_key = generate_private_key()
    public_key = create_public_key(private_key)

    if action == "E":  # Encrypt
        data = get_input(binary=True)
        print("* Transform *")
        chunks = encrypt_mh(data, public_key)
        output = " ".join(map(str, chunks))
    else:  # Decrypt
        data = get_input(binary=False)
        chunks = [int(line.strip()) for line in data.split() if line.strip()]
        print("* Transform *")
        output = decrypt_mh(chunks, private_key)

    set_output(output)


def run_scytale():
    """run Scytale cipher"""
    action = get_action()
    encrypting = action == "E"
    binary_data = _get_selection(
        "Do you want to read binary data? (Y)es or (N)o ", "YN"
    )

    if binary_data == "Y":
        data = get_input(binary=True)
    else:
        data = clean_scytale(get_input(binary=False))

    print("* Transform *")
    while True:
        try:
            circumference = int(input("Circumference? ").strip())
            break
        except ValueError:
            print("This is not a number! Try again!")

    if binary_data == "Y" and encrypting:
        output = encrypt_scytale_binary_data(data, circumference)
        set_output(output, binary=True)
    else:
        output = (encrypt_scytale if encrypting else decrypt_scytale)(
            data, circumference)
        set_output(output)


def run_railfence():
    """run Railfence cipher"""
    action = get_action()
    encrypting = action == "E"
    binary_data = _get_selection(
        "Do you want to read binary data? (Y)es or (N)o ", "YN"
    )

    if binary_data == "Y":
        data = get_input(binary=True)
    else:
        data = clean_railfence(get_input(binary=False))

    print("* Transform *")
    while True:
        try:
            number_of_rails = int(input("Rail? ").strip())
            break
        except ValueError:
            print("This is not a number! Try again!")

    if binary_data == "Y" and encrypting:
        output = encrypt_railfence_binary_data(data, number_of_rails)
        set_output(output, binary=True)
    else:
        output = (encrypt_railfence if encrypting else decrypt_railfence)(
            data, number_of_rails
        )
        set_output(output)


def run_suite():
    """
    Runs a single iteration of the cryptography suite.

    Asks the user for input text from a string or file, whether to encrypt
    or decrypt, what tool to use, and where to show the output.
    """
    print("-" * 34)
    tool = get_tool()
    # This isn't the cleanest way to implement functional control flow,
    # but I thought it was too cool to not sneak in here!
    commands = {
        "C": run_caesar,  # Caesar Cipher
        "V": run_vigenere,  # Vigenere Cipher
        "M": run_merkle_hellman,  # Merkle-Hellman Knapsack Cryptosystem
        "S": run_scytale,  # Scytale Cipher
        "R": run_railfence,  # Railfence Cipher
    }
    commands[tool]()


def main():
    """Harness for CS41 Assignment 1"""
    print("Welcome to the Cryptography Suite!")
    run_suite()
    while get_yes_or_no("Again?"):
        run_suite()
    print("Goodbye!")


if __name__ == "__main__":
    main()
