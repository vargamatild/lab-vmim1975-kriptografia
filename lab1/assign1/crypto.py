#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Name: VARGA MATILD - KATALIN
ID: vmim1975

Implementation of Caesar, Vigenere, Scytale and Railfence Cyphers
- encryption and decryption.
"""
import string


# Caesar Cipher


def get_letter_to_number_dictionary():
    """szotarat terit vissza, amelyben minden nagybetunek megfelel egy szam 0-25 kozott"""

    return dict(zip(string.ascii_uppercase, range(26)))


def get_number_to_letter_dictionary():
    """szotarat terit vissza, amelyben minden szamnak 0-25 kozott megfelel egy nagybetu"""

    return dict(zip(range(26), string.ascii_uppercase))


def encrypt_caesar_binary_data(byte_array):
    """binaris adatokat enkriptal Caesar-al
    256-al modulozunk mert annyi karakter van az ascii tablazatban"""

    ciphertext = ""

    for char in byte_array:
        ciphertext += chr((char + 3) % 256)

    return ciphertext


def encrypt_caesar(plaintext):
    """Encrypt plaintext using a Caesar cipher.
    -modulo 26, mert 26 nagybetu van az angol abc-ben
    -megnezem ha az eloallitott szotarban benne van-e az adott karakter, ha igen akkor
    megfeleltetem neki a megfelelo eltolt karaktert, kulonben ugy hagyom"""

    letter_to_number_dictionary = get_letter_to_number_dictionary()
    number_to_letter_dictionary = get_number_to_letter_dictionary()

    ciphertext = ""
    for char in plaintext:
        if char in letter_to_number_dictionary:
            shifted_char_ord = letter_to_number_dictionary[char] + 3
            if shifted_char_ord > 25:
                shifted_char_ord -= 26
            ciphertext += number_to_letter_dictionary[shifted_char_ord]
        else:
            ciphertext += char

    return ciphertext


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.
    -megnezem ha az eloallitott szotarban benne van-e az adott karakter, ha igen akkor
    megfeleltetem neki a megfelelo eltolt karaktert, kulonben ugy hagyom"""

    letter_to_number_dictionary = get_letter_to_number_dictionary()
    number_to_letter_dictionary = get_number_to_letter_dictionary()

    plaintext = ""
    for char in ciphertext:
        if char in letter_to_number_dictionary:
            shifted_char_ord = letter_to_number_dictionary[char] - 3
            if shifted_char_ord < 0:
                shifted_char_ord += 26
            plaintext += number_to_letter_dictionary[shifted_char_ord]
        else:
            plaintext += char

    return plaintext


# Vigenere Cipher


def repeat_word(word, length):
    """eloallit egy olyan karakterlancot ami a szo ismetlesebol all,
    addig amig a karakterek szama eleri a parameterlistaban megadott szamot"""
    nr_of_repeats = length // len(word) + 1
    repeated_word = word * nr_of_repeats
    return repeated_word[0:length]


def encrypt_vigenere_binary_data(byte_array, keyword):
    """binaris adatokat enkriptal Vigenere-el
    256-al modulozunk mert annyi karakter van az ascii tablazatban"""
    key = repeat_word(keyword, len(byte_array))
    ciphertext = ""

    for i, char in enumerate(byte_array):
        ciphertext += chr((char + key[i]) % 256)

    return ciphertext


def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.
    -eloallitom a kulcsot, ami egyenlo hosszu mint a megadott szoveg
    -megnezem ha az eloallitott szotarban benne van-e az adott karakter, ha igen akkor
    megfeleltetem neki a megfelelo eltolt karaktert s hozzaadom az eloallitott kulcsban
    levo megfelelo karakternek megfelelo szamot"""

    key = repeat_word(keyword, len(plaintext))
    letter_to_number_dictionary = get_letter_to_number_dictionary()
    number_to_letter_dictionary = get_number_to_letter_dictionary()

    ciphertext = ""
    for i, char in enumerate(plaintext):
        shifted_char_ord = (
            letter_to_number_dictionary[char] +
            letter_to_number_dictionary[key[i]]
        )
        if shifted_char_ord >= 26:
            shifted_char_ord -= 26
        ciphertext += number_to_letter_dictionary[shifted_char_ord]

    return ciphertext


def decrypt_vigenere(ciphertext, keyword, ):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.
    -eloallitom a kulcsot, ami egyenlo hosszu mint a megadott szoveg
    -megnezem ha az eloallitott szotarban benne van-e az adott karakter, ha igen akkor
    megfeleltetem neki a megfelelo eltolt karaktert s hozzaadom az eloallitott kulcsban
    levo megfelelo karakternek megfelelo szamot"""

    key = repeat_word(keyword, len(ciphertext))
    letter_to_number_dictionary = get_letter_to_number_dictionary()
    number_to_letter_dictionary = get_number_to_letter_dictionary()

    plaintext = ""
    for i, char in enumerate(ciphertext):
        shifted_char_ord = (
            letter_to_number_dictionary[char] -
            letter_to_number_dictionary[key[i]]
        )
        if shifted_char_ord < 0:
            shifted_char_ord += 26
        plaintext += number_to_letter_dictionary.get(shifted_char_ord)

    return plaintext


# Scytale Cipher


def encrypt_scytale_binary_data(byte_array, circumference):
    """Encrypt binary data using a Scytale cipher.
    -feltoltom a szo veget pontokkal, hogy legyen a szo hossza oszthato a circumference-el,
    s aztan csak ugralok circumference-nyi tavolsagokat a karakterlancban"""

    if len(byte_array) % circumference != 0:
        byte_array += bytes("".join(
            ["." for _ in range(
                circumference - len(byte_array) % circumference)]), "utf-8")

    return b"".join([byte_array[i::circumference] for i in range(circumference)])


def encrypt_scytale(plaintext, circumference):
    """Encrypt plaintext using a Scytale cipher.
    -feltoltom a szo veget pontokkal, hogy legyen a szo hossza oszthato a circumference-el,
    s aztan csak ugralok circumference-nyi tavolsagokat a karakterlancban"""

    if len(plaintext) % circumference != 0:
        plaintext += "".join(
            ["." for _ in range(
                circumference - len(plaintext) % circumference)]
        )

    return "".join([plaintext[i::circumference] for i in range(circumference)])


def decrypt_scytale(ciphertext, circumference):
    """Decrypt ciphertext using a Scytale cipher.
    -ha a szo hossza nem oszthato a circumference-el akkor azt jelenti egyel tobb kell legyen a
    circumference, amire viszont meghivom az enkriptalo algoritmust az uj circumference-el
    (ha matrix formaban leirjuk az enkriptalast akkor a circumference a sort adja meg,
    dekriptalasnal az oszlopszamra van szuksegunk)"""

    new_circumference = len(ciphertext) // circumference
    if len(ciphertext) % circumference != 0:
        new_circumference += 1

    return encrypt_scytale(ciphertext, new_circumference).replace(".", "")


# Railfence Cipher


def get_item(i, num_rails, plaintext):
    """-visszaterit egy olyan karakterlancot ami a Railfence kodolassal leirt matrixban
    egy egy sort reprezental.
    -vegulis cikk-cakkban bejarom a szonak a karaktereit hogy megkapjam a kello 'matrixsort'
    -tudom hogy (i-1)*2-vel es (num_rails-1)*2-(i-1)*2 kellene ugralni, felvaltva"""
    if i == num_rails:
        return plaintext[(num_rails - i):: ((i - 1) * 2)]

    if i == 1:
        return plaintext[(num_rails - i):: (num_rails - 1) * 2]

    step1 = (i - 1) * 2
    step2 = (num_rails - 1) * 2 - step1
    step1_used = False
    j = num_rails - i
    plaintext_lenght = len(plaintext)
    ciphertext = ""

    while j < plaintext_lenght:
        ciphertext += plaintext[j]
        if step1_used:
            j += step2
            step1_used = False
        else:
            j += step1
            step1_used = True

    return ciphertext


def get_item_for_binary(i, num_rails, plaintext):
    """-visszaterit egy olyan bytesorozatot ami a Railfence kodolassal leirt matrixban
    egy egy sort reprezental.
    -vegulis cikk-cakkban bejarom a szonak a karaktereit hogy megkapjam a kello 'matrixsort'
    -tudom hogy (i-1)*2-vel es (num_rails-1)*2-(i-1)*2 kellene ugralni, felvaltva"""
    if i == num_rails:
        return plaintext[(num_rails - i):: ((i - 1) * 2)]

    if i == 1:
        return plaintext[(num_rails - i):: (num_rails - 1) * 2]

    step1 = (i - 1) * 2
    step2 = (num_rails - 1) * 2 - step1
    step1_used = False
    j = num_rails - i
    plaintext_lenght = len(plaintext)
    ciphertext = ""

    while j < plaintext_lenght:
        ciphertext += chr(plaintext[j])
        if step1_used:
            j += step2
            step1_used = False
        else:
            j += step1
            step1_used = True

    return bytes(ciphertext, "utf-8")


def encrypt_railfence_binary_data(byte_array, num_rails):
    """Encrypt binary data using a Railfence cipher."""
    return b"".join(
        [
            get_item_for_binary(i, num_rails, byte_array)
            for i in range(num_rails, 0, -1)
        ]
    )


def encrypt_railfence(plaintext, num_rails):
    """Encrypt plaintext using a Railfence cipher.
    konkatenalom a get_item fgv-vel visszakapott 'matrixsorokat' """

    return "".join(
        [
            get_item(i, num_rails, plaintext)
            for i in range(num_rails, 0, -1)
        ]
    )


def decrypt_railfence(ciphertext, num_rails):
    """Decrypt ciphertext using a Railfence cipher.
    -eloallitom a matrixot, megjelolom cikk-cakk-ban hogy hova kellene egyalatalan valami karakter
    keruljon (*)
    -aztan a karakterlancon athaladva s a matrixon is egyszerre a *-k helyere beirom sorba a
    kovetkezo betut
    -aztan cikk-cakkban haladva a matrixban 'kiolvasom' a megfejtett szoveget"""

    matrix = [["." for i in range(len(ciphertext))] for j in range(num_rails)]
    row = 0
    down = True

    for col in range(len(ciphertext)):
        if row == num_rails - 1:
            down = False
        elif row == 0:
            down = True

        matrix[row][col] = "*"

        if down:
            row += 1
        else:
            row -= 1

    k = 0
    for row in range(num_rails):
        for col in range(len(ciphertext)):
            if matrix[row][col] == "*":
                matrix[row][col] = ciphertext[k]
                k += 1

    row = 0
    down = True
    plaintext = ""

    for col in range(len(ciphertext)):
        if row == num_rails - 1:
            down = False
        elif row == 0:
            down = True

        plaintext += matrix[row][col]

        if down:
            row += 1
        else:
            row -= 1

    return plaintext


# Merkle-Hellman Knapsack Cryptosystem

# pylint: disable=invalid-name
def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in
    Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here
